export { authPinia } from "./auth"
export { dataPinia } from "./data"
export * as Communicate from './communicate'