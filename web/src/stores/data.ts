import { defineStore } from 'pinia'
import { authPinia } from './auth'
import { server_post, configCode } from '@/http';
export const dataPinia = defineStore('dataPinia', () => {
    const messageObj: any = ref({})
    const getMessageList = async () => {
        const authStore = authPinia()
        const { data }: any = await server_post('/testApi/getMessageList')
        messageObj.value = data
        authStore.providerOptions.msg = data.list.some((n) => {
            return !n.read
        })
        return data
    }
    const getReadMessage = async (id: number) => {
        await server_post('/testApi/getReadMessage', { id: id })
    }
    const setAvatar = async (params) => {
        const { code } = await server_post('/testApi/setAvatar', params)
        if (code === configCode.OK) {
            const authStore = authPinia()
            authStore.getUsers()
            return true
        }
        return false
    }
    return {
        messageObj,
        getMessageList,
        getReadMessage,
        setAvatar,
    }
})