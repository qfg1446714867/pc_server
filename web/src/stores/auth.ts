import { defineStore } from 'pinia'
import { useSessionStorage, useLocalStorage } from '@vueuse/core';
import zhCN from 'ant-design-vue/es/locale/zh_CN';
import enUS from 'ant-design-vue/es/locale/en_US';
import 'dayjs/locale/zh-cn';
import { blobGoBase64 } from '@/utils'
import { server_post } from '@/http';
import config from '../../package.json'
import router from '@/router/index.js'
const localeOptions: any = {
    'zhCN': zhCN,
    'enUS': enUS
}
export const authPinia: any = defineStore('authPinia', () => {
    const defaultColor = '#4f46e5'
    const whiteRouteList = ref(['/login', '/oneClickLogin'])
    const token = useSessionStorage('token', null)
    const color = useSessionStorage('color', defaultColor)
    const userData: any = useSessionStorage('userData', {})
    const menuList = useSessionStorage('menuList', [])
    const currentRouter = useSessionStorage('currentRouter', {})
    const tabList: any = useSessionStorage('tabList', [])
    const locale = useLocalStorage('locale', 'zhCN')
    const noticeCount = ref(0)
    const providerOptions: any = reactive({
        name: config.name,
        version: config.version,
        isOpenMenuDrawer: false,
        msg: false,
        theme: {
            token: {
                colorPrimary: color.value,
                colorError: '#ff4d4f',
                colorInfo: '#1677ff',
                colorSuccess: '#52c41a',
                colorWarning: '#faad14',
                colorBar: '#e3e3e3',
                fontFamily: '',
                fontSize: 12,
                size: 30,
                borderRadius: '6px',
            },
            inherit: {},
        },
        colorList: function () {
            return new Set([color.value, defaultColor, '#f00', '#00f', '#f0f'])
        },
        autoInsertSpaceInButton: true,
        componentSize: 'middle', //small | middle | large
        space: 'small', //small | middle | large | number
        locale: localeOptions[locale.value],
        layoutOption: {
            showHead: true,
            showBread: true,
            defaultMenuCollapsed: false,
            tabTransNum: 200
        },
    })
    const getMenus = async (bool?: boolean) => {
        if (bool) {
            const { data }: any = await server_post('/testApi/menu')
            menuList.value = data
        }
        await changeRouterMenu(menuList.value)
    }
    const getUsers = async () => {
        const { data }: any = await server_post('/testApi/user')
        userData.value = data
        server_post('/testApi/getAvatar', {}, { responseType: 'blob' }).then(async (res) => {
            const avatar = await blobGoBase64(res)
            userData.value.avatar = avatar
        })
    }
    // 退出登录
    const exit = () => {
        token.value = null
        router.replace('/login')
        let timer = setTimeout(() => {
            userData.value = {}
            menuList.value = []
            tabList.value = []
            currentRouter.value = {}
            noticeCount.value = 0
            clearTimeout(timer)
        }, 500);
    }
    // 添加路由
    var changeRouterMenu = async (list: any) => {
        const moduleVue = import.meta.glob('@/views/**/*.vue')
        const moduleTsx = import.meta.glob('@/views/**/*.tsx')
        function fors(list) {
            list.map((item) => {
                const rout = routerStructure(item, moduleVue, moduleTsx)
                if (!rout.meta.parent) {
                    router.addRoute('', rout)
                } else {
                    router.addRoute('LAYOUT', rout)
                }
                if (item.children && item.children.length > 0) fors(item.children)
            })
        }
        fors(list)
        const noRoute = [{ path: '/:catchAll(.*)', redirect: '/404', }, { path: '/:pathMatch(.*)*', redirect: '/404', }, { path: '/:pathMatch(.*)', redirect: '/404', }]
        noRoute.forEach((item) => {
            router.addRoute('', item)
        })
    }
    // 处理路由
    var routerStructure = (item, moV, moT) => {
        const com = moV[`/src${item.url}/index.vue`] || moT[`/src${item.url}/index.tsx`]
        const params: any = {
            path: item.path,
            name: item.name,
            component: com,
            meta: {
                title: item.title,
                parent: item.parent,
                icon: item.icon,
                keepAlive: !!item.keepAlive,
                show: item.show
            },
            children: []
        }
        if (com === undefined && (!item.children || (item.children && item.children.length < 1))) {
            params.redirect = '/404'
        }
        if (item.children && item.children.length > 0) {
            item.children.map((item2) => {
                params.children.push(routerStructure(item2, moV, moT))
            })
        }
        return params
    }
    return {
        color,
        token, // token
        userData, // 权限
        menuList, // 菜单列表
        tabList, // tab列表
        locale, // 语言
        noticeCount, // 提示消息数
        providerOptions, // 组件样式
        getMenus, // 获取菜单
        getUsers,
        exit, // 退出
        whiteRouteList, // 白名单
        currentRouter, // 当前路由
    }
})