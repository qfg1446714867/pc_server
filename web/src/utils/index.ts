
export const defaultPathActive = (route) => {
    const partString = route.path.split('/')
    const arr: (string | undefined)[] = []
    let currentPath: string = '';
    for (let i = 1; i < partString.length; i++) {
        currentPath += '/' + partString[i];
        if (i < partString.length - 1) {
            arr.unshift(currentPath);
        }
    }
    arr.unshift(partString[0]);
    return arr
}
// 保留routItem
export function storeRouterItem(row) {
    return { path: row.path, query: row.query, params: row.params, meta: row.meta }
}
/**
 * 深拷贝
 * @param data
 * @returns data
 */
export function deepClone(target) {
    try {
        return structuredClone(target)
    } catch {
        let result
        if (typeof target === 'object') {
            if (Array.isArray(target)) {
                result = []
                for (let i in target) {
                    result.push(deepClone(target[i]))
                }
            } else if (target === null) {
                result = null
            } else if (target.constructor === RegExp) {
                result = target
            } else {
                result = {}
                for (let i in target) {
                    result[i] = deepClone(target[i])
                }
            }
        } else {
            result = target
        }
        return result
    }
}
/**
 * 颜色改变
 * @param color #fff #ffffff rgb 
 * @param opacity 透明度
 * @returns rgba
 */
export function colorOpacity(color: string, opacity: number) {
    if (color.indexOf('rgba') > -1) return color
    if (color.indexOf('rgb') > -1) {
        return color.replace('rgb', 'rgba').replace(')', `,${opacity})`);
    }
    if (color.indexOf('#') > -1) {
        if (color.length < 5) {
            const r = color.slice(1, 2) + color.slice(1, 2)
            const g = color.slice(2, 3) + color.slice(2, 3)
            const b = color.slice(3, 4) + color.slice(3, 4)
            const c = `#${r}${g}${b}`
            const sl = (o, f) => parseInt(c.slice(o, f), 16)
            return `rgba(${sl(1, 3)},${sl(3, 5)}, ${sl(5, 7)},${opacity})`;
        }
        const sl = (o, f) => parseInt(color.slice(o, f), 16)
        return `rgba(${sl(1, 3)},${sl(3, 5)}, ${sl(5, 7)},${opacity})`;
    }
}
/**
 * blob转换base64
 * @param val blob文件 
 * @returns base64文件
 */
export function blobGoBase64(val: Blob) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        try {
            reader.onload = function () {
                const dataUrl = reader.result;
                resolve(dataUrl)
            };
            reader.readAsDataURL(val)
        } catch {
            reject(false)
        }
    })
}
/**
 * file转换blob
 * @param val 文件 
 * @returns base64文件
 */
export function fileGoblob(val: any) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.readAsArrayBuffer(val)
        reader.onload = function (e: any) {
            let blob: any = null
            if (typeof e.target.result === 'object') {
                blob = new Blob([e.target.result], { type: val.type })
            } else {
                blob = e.target.result
            }
            resolve({ name: val.name, blob: blob })
        }
    })
}
/**
 * 
 * @param num number
 * @returns 
 */
export const thousandFixed = (num: any, lang: 'zh' | 'en' = 'zh') => {
    if (!num) {
        num = 0;
    }
    num - 0;
    if (num > 9999 && num < 100000000) {
        let nums: any = num / 10000
        // console.log('万')
        return (nums.toFixed(0).toString()).replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,')+ `${lang ==='en'? 'w': '万'}`;
    } else if (num > 99999999) {
        let nums: any = num / 100000000
        // console.log('亿')
        return (nums.toFixed(0).toString()).replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,')+ `${lang ==='en'? 't': '亿'}`;
    } else {
        return num;
    }
}