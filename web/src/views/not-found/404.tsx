import { defineComponent } from "vue"
import { useRouter } from 'vue-router'
import { styled } from '@styils/vue'
import NOT404 from '@/assets/images/not_404.png'
export default defineComponent({
    setup() {
        const router = useRouter()
        const not404Content = styled('div', {
            width: '100%',
            height: '100%',
            display: 'flex',
            flexFlow: 'column nowrap',
            justifyContent: 'center',
            alignItems: 'center',
        })
        const not404ContentBtn = styled('div', {
            width: '50vw',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        })
        const go = (type?: boolean) => {
            if (type) return router.push('/')
            router.go(-1)
        }
        return () => (<not404Content>
            <img style={{ width: '50vw' }} src={NOT404} />
            <not404ContentBtn>
                <span style={{ margin: '0 12px' }} onClick={() => go(true)}>返回首页</span>
                <span style={{ margin: '0 12px' }} onClick={() => go()}>返回上一页</span>
            </not404ContentBtn>
        </not404Content>)
    }
})