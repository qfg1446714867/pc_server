import { PayCircleOutlined, UsergroupDeleteOutlined, HeartOutlined, MoneyCollectOutlined } from '@ant-design/icons-vue'
import { authPinia } from '@/stores'
import { server_post } from '@/http';
import dayJS from 'dayjs'
const authStore = authPinia()
const { colorError, colorSuccess, colorWarning } = authStore.providerOptions.theme.token
export const newDayData = ref([
    { key: 'daySale', prefix: '$', label: '今日的销售', value: '0', percentage: 0, icon: PayCircleOutlined },
    { key: 'dayUser', prefix: null, label: '今日的用户', value: '0', percentage: 0, icon: UsergroupDeleteOutlined },
    { key: 'orderUser', prefix: '+', label: '新用户', value: '0', percentage: 0, icon: HeartOutlined },
    { key: 'orderForm', prefix: '', label: '新订单', value: '0', percentage: 0, icon: MoneyCollectOutlined },
])
export const chartData: any = reactive({
    activeUser: {
        xData: [],
        yData: [],
        percentage: 0,
        describe: '',
        title: '活动用户',
        detail: [
            { key: 'user', label: '用户', value: '0' },
            { key: 'click', label: '点击量', value: '0' },
            { key: 'sale', label: '售额', value: '0' },
            { key: 'item', label: '详细', value: '0' }
        ]
    },
    salesData: {
        xData: [],
        yData: [],
        title: '销售概况',
        percentage: 0,
    }

})
export const tableData: any = ref({
    title: '项目',
    doneMonth: 0,
    list: [],
})
export const historyData: any = ref({
    title: '历史订单',
    doneMonth: 0,
    list: [],
})

export const getNewDay = async () => {
    const { data } = await server_post('/testApi/getNewDay')
    newDayData.value.map((item) => {
        item.value = data[item.key].value;
        item.percentage = data[item.key].percentage;
        return item
    })
}
export const getSalesData = async () => {
    const end = dayJS().subtract(1, 'day').format('YYYY-MM-DD')
    const start = dayJS(end).subtract(7, 'day').format('YYYY-MM-DD')
    const { data } = await server_post('/testApi/getSalesData', { startData: start, endData: end })
    const { activeUser, salesData } = data
    if (activeUser) {
        chartData.activeUser.percentage = activeUser.percentage;
        chartData.activeUser.describe = activeUser.describe;
        chartData.activeUser.detail.forEach((n) => {
            n.value = activeUser.detail[n.key];
        })
        chartData.activeUser.xData = activeUser.list.map(n => n.name)
        chartData.activeUser.yData = activeUser.list
    }
    if (salesData) {
        chartData.salesData.percentage = salesData.percentage;
        chartData.salesData.xData = salesData.name
        chartData.salesData.yData = []
        for (const keyI in salesData.data) {
            chartData.salesData.yData.push({
                name: keyI,
                value: salesData.data[keyI]
            })
        }
    }
}
export const getTableData = async () => {
    const { data } = await server_post('/testApi/getTableData')
    tableData.value = {
        ...tableData.value,
        ...data
    };
}
export const getHistoryData = async () => {
    const { data } = await server_post('/testApi/getHistoryData')
    historyData.value = {
        ...historyData.value,
        ...data
    };
}
export {
    authPinia,
    colorError as errorColor,
    colorSuccess as successColor,
    colorWarning as warningColor
}