import { createAlova, useRequest, useWatcher } from 'alova';
import VueHook from 'alova/vue';
import GlobalFetch from 'alova/GlobalFetch';
import { errorCode } from '../code';
import { message as AntMessage } from 'ant-design-vue';
const server_alova = createAlova({
    statesHook: VueHook,
    requestAdapter: GlobalFetch(),
    baseURL: '/',
    timeout: 60000,
    beforeRequest(_method: any) {
        const token = sessionStorage.getItem('token')
        if (token) {
            _method.config.headers['Authorization'] = token 
            _method.data = _method.data
        }
    },
    responded: {
        onSuccess: async (response, _method: any) => {
            if (errorCode[response.status]) {
                AntMessage.error(errorCode[response.status]);
                return null
            }
            let json: any = null
            if (_method.config.responseType === 'blob') {
                json = await response.blob();
            } else {
                json = await response.json();
            }
            return json;
        },
        onError: async () => {}
    }
});
// https://alova.js.org/zh-CN/tutorial/getting-started/states-change-request
/**
 * get
 * @param url 路径
 * @param options 携带，配置参数
 * @returns get请求
 */
const server_get = (url: string, options?: any) => server_alova.Get(url, options)
/**
 * post
 * @param url 路径
 * @param body 携带参数
 * @param options 配置参数
 * @returns post请求
 */
const server_post = (url: string, body?: { [key: string]: any }, options?: any) => server_alova.Post(url, body, options)
export {
    server_alova, server_get, server_post, useRequest, useWatcher
}