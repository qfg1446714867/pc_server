export default class SCOKET {
    private url: string = '';
    private id: string = '';
    private scoketVal: any = null;
    private timer: any = null
    constructor({ url, id }) {
        this.url = url;
        this.id = id;
    }
    open(callback: any) {
        this.scoketVal = new WebSocket(this.url);
        this.scoketVal.onopen = () => {
            console.log("=====开启链接-web=====");
            this.scoketVal.addEventListener('message', (e) => {
                const isStr = typeof e.data === 'string'
                callback(isStr ? JSON.parse(e.data) : e.data)
            })
        }
    }
    send(data: string, again?: boolean) {
        const str = JSON.stringify({ again: again ? 1 : 0, id: this.id, data });
        this.scoketVal.send(str)
    }
    close() {
        this.scoketVal.onclose = () => {
            console.log('=====关闭链接-close=====');
            this.scoketVal.removeEventListener('message', (e) => { })
            this.clear()
        }
    }
    error() {
        this.scoketVal.onerror = () => {
            console.log('链接失败-web');
            this.clear()
        }
    }
    private clear() {
        if (this.timer) {
            clearInterval(this.timer)
            this.timer = null
            this.url = ''
            this.id = ''
            this.scoketVal = null
        }
    }

}