import router from './index.js'
import { authPinia } from '../stores'
import { storeRouterItem } from '@/utils'
router.beforeEach(async (_to: any, _from: any, _next: any) => {
    const authStore = authPinia()
    const token = !!authStore.token
    if (token) {
        if (['/login', '/'].includes(_to.path)) {
            _next({ path: '/', replace: true })
        } else if (_to.matched.length === 0) {
            await authStore.getMenus()
            _next(authStore.currentRouter)
        } else {
            authStore.currentRouter = storeRouterItem(_to)
            _next()
        }
    } else {
        _to.path === '/login' || authStore.whiteRouteList.some(item => item === _to.path) ? _next() : _next('/login')
    }
})
router.afterEach((_to: any) => {
})