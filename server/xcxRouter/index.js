const express = require('express')
const { getTimeoutNum, not404999999 } = require('../utils/getArg.js')
const router = express.Router()
const data = require('../data/xcxData.json')
// 获取主题
router.post('/getThemeList', async (req, res) => {
    await getTimeoutNum()
    const value = data.theme
    console.log(value, 'valuevaluevalue');
    res.send({
        code: 200000,
        message: '请求成功！',
        data: value
    })
    return
    not404999999(res, '请求失败！')
})
// 获取轮播图推荐
router.post('/getSwiperList', async (req, res) => {
    await getTimeoutNum()
    const value = [
        {
            "img": "",
            "id": "1"
        },
        {
            "img": "",
            "id": "2"
        },
        {
            "img": "",
            "id": "3"
        },
        {
            "img": "",
            "id": "4"
        }
    ]
    console.log(value, 'valuevaluevalue');
    res.send({
        code: 200000,
        message: '请求成功！',
        data: value
    })
    return
    not404999999(res, '请求失败！')
})
module.exports = router