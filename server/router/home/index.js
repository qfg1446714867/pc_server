const express = require('express')
const dayjs = require('dayjs')
const { getBodyCall, getTimeoutNum, serverPath, not404999999, validateToken } = require('../../utils/getArg.js')
const router = express.Router()
const data = require('../../data/home.json')
const user = require('../../data/index.json')
// 获取今日额
router.post('/getNewDay', async (req, res) => {
    await getTimeoutNum()
    const { token } = await getBodyCall(req);
    const itemUserData = await validateToken(user, token);
    if (itemUserData) {
        const value = data.newDay
        res.send({
            code: 200000,
            message: '请求成功！',
            data: value
        })
        return
    }
    not404999999(res, '请求失败！')
})
// 获取销售
router.post('/getSalesData', async (req, res) => {
    await getTimeoutNum()
    const { token, startData, endData } = await getBodyCall(req);
    const itemUserData = await validateToken(user, token);
    if (itemUserData) {
        const unitTime = new Date(endData) - new Date(startData)
        const unit = unitTime / 1000 / 60 / 60 / 24
        const activeUserList = Array.from(new Array(unit)).map((_item, i) => {
            const day = dayjs(startData).subtract(i + 1, 'day').format('YYYY-MM-DD')
            return {
                name: day,
                value: Math.floor(Math.random() * 5000)
            }
        })
        const nameList = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月',]
        const value1 = Array.from(new Array(12)).map((_item, i) => Math.floor(Math.random() * 5000))
        const value2 = Array.from(new Array(12)).map((_item, i) => Math.floor(Math.random() * 5000))
        const activeUser = {
            ...data.activeUser,
            percentage: (activeUserList.map(n => n.value).reduce((a, b) => a + b, 0) / data.activeUser.detail.sale * 100).toFixed(2),
            list: activeUserList,
        }
        const salesData = {
            ...data.salesData,
            name: nameList,
            data: {
                Traffic: value1,
                Sales: value2
            }
        }
        res.send({
            code: 200000,
            message: '请求成功！',
            data: {
                activeUser,
                salesData
            }
        })
        return
    }
    not404999999(res, '请求失败！')
})
// 获取表格信息
router.post('/getTableData', async (req, res) => {
    await getTimeoutNum()
    const { token } = await getBodyCall(req);
    const itemUserData = await validateToken(user, token);
    if (itemUserData) {
        const unit = Math.floor(Math.random() * 10)
        const nameList = ['Soft UI Shopify Version', 'Progress Track', 'Fix Platform Errors', 'Launch new', 'APP', 'PROMISE GO', 'MARKETING', 'SEO', 'SOCIAL', 'ONLINEOL']
        const list = Array.from(new Array(unit)).map((_item, i) => {
            return {
                COMPLETION: Math.floor(Math.random() * 100),
                BUDGET: Math.floor(Math.random() * 5000),
                COMPANIES: nameList[i]
            }
        })
        res.send({
            code: 200000,
            message: '请求成功！',
            data: {
                ...data.doneMonthData,
                list: list,
            }
        })
        return
    }
    not404999999(res, '请求失败！')
})
// 获取历史
router.post('/getHistoryData', async (req, res) => {
    await getTimeoutNum()
    const { token } = await getBodyCall(req);
    const itemUserData = await validateToken(user, token);
    if (itemUserData) {
        const unit = Math.floor(Math.random() * 10)
        const nameList = ['Soft UI Shopify Version', 'Progress Track', 'Fix Platform Errors', 'Launch new', 'APP', 'PROMISE GO', 'MARKETING', 'SEO', 'SOCIAL', 'ONLINEOL']
        const list = Array.from(new Array(unit)).map((_item, i) => {
            return {
                time: dayjs(new Date()).subtract(i + 1, 'day').format('YYYY-MM-DD'),
                value: nameList[Math.floor(Math.random() * 10)]
            }
        })
        res.send({
            code: 200000,
            message: '请求成功！',
            data: {
                ...data.historyData,
                list: list,
            }
        })
        return
    }
    not404999999(res, '请求失败！')
})

module.exports = router