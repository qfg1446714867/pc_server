const express = require('express')
const fs = require('fs')
const multer = require('multer');
const { Readable } = require('stream');
const { getBodyCall, getTimeoutNum, not404999999, serverPath, validateToken } = require('../../utils/getArg.js')
const router = express.Router()
const uploads = multer({ dest: serverPath });
const data = require('../../data/index.json')
// user
router.post('/user', async (req, res) => {
    await getTimeoutNum()
    const { token } = await getBodyCall(req)
    const itemUserData = await validateToken(data, token)
    const obj = {
        ...itemUserData.user,
    }
    delete obj.password
    delete obj.avatar
    if (itemUserData) {
        res.send({
            code: 200000,
            message: '获取成功！',
            data: obj
        })
        return
    }
    not404999999(res, '获取失败！')
})
router.post('/getAvatar', async (req, res) => {
    await getTimeoutNum()
    const { token } = await getBodyCall(req)
    const itemUserData = await validateToken(data, token)
    if (itemUserData) {
        const url = serverPath + itemUserData.user.avatar
        res.sendFile(url);
        return
    }
    not404999999(res, '获取失败！')
})
// 菜单
router.post('/menu', async (req, res) => {
    await getTimeoutNum()
    const { token } = await getBodyCall(req)
    const itemUserData = await validateToken(data, token)
    if (itemUserData) {
        res.send({
            code: 200000,
            message: '获取权限成功！',
            data: itemUserData.menu
        })
        return
    }
    not404999999(res, '获取菜单失败！')
})

// 
router.post('/setAvatar', uploads.single('avatar'), async (req, res) => {
    await getTimeoutNum()
    const { token, base, name } = await getBodyCall(req)
    const itemUserData = await validateToken(data, token)
    if (itemUserData) {
        const newBase = base.replace(/^data:image\/\w+;base64,/, "")
        const base64Stream = new Readable();
        base64Stream.push(newBase, 'base64');
        base64Stream.push(null); // 设置流结束
        base64Stream.pipe(fs.createWriteStream(`${serverPath}/data/images/admin.${'jpg'}`));
        res.send({
            code: 200000,
            message: '修改成功！',
            data: null
        })
        return
    }
    not404999999(res, '修改失败！')
})
module.exports = router