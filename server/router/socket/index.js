
const express = require('express');
const router = express.Router();
const WebSocket = require('ws');
const fs = require('fs')
const dayjs = require('dayjs')
const { serverPath, getParamsCall } = require('../../utils/getArg.js')

// 创建 WebSocket 服务器
const wss = new WebSocket.Server({ noServer: true });
wss.on('connection', async function connection(ws, req) {
    const { createLoad } = await getParamsCall(req)
    if (createLoad) {
        fs.readFile(`${serverPath}/data/${createLoad}.txt`, 'utf8', (err2, data2) => {
            ws.send(JSON.stringify({ id: createLoad, data: data2 }))
        })
    }
    ws.on('message', function incoming(message) {
        console.log('收到消息：', message.toString(), JSON.parse(message.toString()));
        const data = JSON.parse(message.toString())
        if (data.again === 1) {
            fs.writeFile(`${serverPath}/data/${data.id}.txt`, `${data.data}`, (err, _data) => {
                if (err) {
                    console.log('写入错误');
                    ws.send(`写入失败${dayjs().format('YYYY-MM-DD HH:mm:ss')}`)
                    return
                }
                ws.send(JSON.stringify({ id: data.id, data: data.data }))
            })
            return
        }
        fs.appendFile(`${serverPath}/data/${data.id}.txt`, `${data.data}。`, (err, _data) => {
            if (err) {
                console.log('写入错误');
                ws.send(`写入失败${dayjs().format('YYYY-MM-DD HH:mm:ss')}`)
                return
            }
            ws.send(JSON.stringify({ id: data.id, data: data.data }))
        })
    });
    ws.on('close', function () {
        console.log('关闭链接-server');
    });
});

// 导出路由模块
module.exports = { router, wss };