
const express = require('express');
const router = express.Router();
const WebSocket = require('ws');
const fs = require('fs')
const { serverPath, getParamsCall } = require('../../utils/getArg.js')
const data = require('../../data/charts.json')
// 创建 WebSocket 服务器
const chartWs = new WebSocket.Server({ noServer: true });
chartWs.on('connection', async function connection(ws, req) {
    const { } = await getParamsCall(req)
    const newData = {}
    for (const keys in data) {
        newData[keys] = data[keys]
        // 系统百分比
        newData['percentageSystem'] = data['label'].map((_n) => {
            const all = _n.list.map(_m => _m.value).reduce((a, b) => (a + b))
            return {
                value: all,
                name: _n.type
            }
        })
    }
    ws.send(JSON.stringify(newData));
    ws.on('message', function incoming(message) {

    });
    ws.on('close', function () { });
});

// 导出路由模块
module.exports = { router, chartWs };